import React from 'react'

function List(props){
	// console.log(props);
	return(
		<ul>
			{
				props.items.map((items, index) => 
				<li key={index}>{items}</li> )
			}
		</ul>
	);
}

export default List;