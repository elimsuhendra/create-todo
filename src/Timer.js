import React, { Component } from 'react'

class Timer extends Component {
    constructor(props){
        super(props)
        this.state = {
          todoItem: '',
          items: []
        }
    }

    //Lifecycle
    function componentDidMount(){
        this.addInterval = setInterval (()=> this.increase(), 1000)
    }

    function componentWillUnmount(){
        // clearInterval(addInterval);
    }

    function increase(){
        this.setState((state, props)=>({
        time : parseInt(state.time)
        }))
    }
}

export default Timer;