import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import './App.css'
import List from './List'
// import Timer from './Timer.js'

//state
class Timer extends Component {
  constructor(props){
    super(props)
    this.state = {
        time : props.start
    }
  }
  
  //Lifecycle
  componentDidMount(){
    this.addInterval = setInterval (() => this.increase(), 1000)
  }

  componentWillUnmount(){
    clearInterval(this.addInterval);
  }

  increase(){
    this.setState((state, props)=>({
      time : parseInt(state.time) + 1
    }))
  }

  render() {
    return (
    <div>{this.state.time} Detik</div>
    )
  }
}

class App extends Component {
  constructor(){
      super()
      this.state = {
        todoItem: '',
        items: []
      }
  }

  handleSubmit = (e) => {
    e.preventDefault()
    // console.log('Hello Submit')

    this.setState({
      // items       : [...this.state.items, this.state.todoItem],
      items       : this.state.items.concat([this.state.todoItem]),
      todoItem    : ''
    })
  }

  handleChange = (e) =>{
    this.setState({
      todoItem: e.target.value
    })

    console.log(this.state.todoItem);
  }

  render() {
    return (
      <div className="App" onSubmit={this.handleSubmit}>
        <form>
          <input value={this.state.todoItem} onChange={this.handleChange}/>
          <button>Add</button>
        </form>

        <Timer start="0" />
        <List items={this.state.items} />
      </div>
    )
  }
}
export default App